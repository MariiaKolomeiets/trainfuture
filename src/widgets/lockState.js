import React, {Component, Fragment} from 'react';
import {View,  Image} from 'react-native';

export const Lock = (props) => {
    const {state, error, sizes} = props;

   const  renderIcon = () => {
       if(state){
           if(error){
               return  <Image style={{...sizes}} source={require('../../assets/icons/carriage/open-lock-er.png')}/>

           }
           return <Image style={{...sizes}} source={require('../../assets/icons/carriage/open-lock.png')}/>
       } else {
           if(error){
               return <Image style={{...sizes}} source={require('../../assets/icons/carriage/lock-er.png')}/>

           }
            return <Image style={{...sizes}} source={require('../../assets/icons/carriage/lock.png')}/>

       }
   };
    return (
        <View>
            {renderIcon()}
        </View>
    )
};
