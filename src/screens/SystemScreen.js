import React, {Component, Fragment} from 'react';

import {StyleSheet, View, Image, Text, Switch, TouchableWithoutFeedback} from 'react-native';

import {System} from '../containers/system';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

export default class SystemScreen extends Component {
    constructor(){
        super();
        this.state = {
            enabled: true,
            system:null
        }
    }

    componentDidMount(){
       const system = this.props.navigation.getParam('system');
        this.setState({system: system})
    }
    switch = () => {
        const {enabled} = this.state;
        this.setState({enabled: !enabled})
    };
    navigateBack = ()=>{
        this.props.navigation.goBack();
    }
    render() {
        const {system} = this.state;

        return (
            <>
                <View style={styles.navigationBack}>
                    <TouchableWithoutFeedback onPress={this.navigateBack}>
                    <Image style={styles.backBtn} source={require('../../assets/icons/left-arrow.png')}/>
                    </TouchableWithoutFeedback>
                    <Text style={styles.systemCaption}>{system ? (system).toString().toLocaleUpperCase() : ''}</Text>
                    <Switch value={this.state.enabled} style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }] }} onValueChange={this.switch}   trackColor={{true: '#ffc580', false: '#ffc580'}}  />
                </View>
                {system ? (
                    <View style={styles.systemWrap}>
                        <System system={system}/>
                    </View>
                ):null}



            </>
        );
    }
}

const styles = StyleSheet.create({
    navigationBack:{
        paddingVertical:20,
        paddingHorizontal: 15,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    systemWrap:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width: '100%',
        marginTop:20
    },
    backBtn:{
        height:15,
        width:30,
    },
    systemCaption:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(5),
    }
});
