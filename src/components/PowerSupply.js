import React, {Component, Fragment} from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback, FlatList, Modal} from 'react-native';
import CircularSlider from './CircleSlider';
import Slider from '@react-native-community/slider';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"
export default class PowerSupply  extends Component{
    constructor(props) {
        super(props);
        this.state = {
            modalInfo: false
        };
    }
    renderItemList = (item)=> {
        return (
            <View style={{width:'100%', flexDirection:'row',alignItems:'center', justifyContent:'center'}}>
                <View style={styles.infoBlock}>
                    <View>
                        <Text style={styles.infoText}>{item.item} </Text>
                    </View>
                    <View>
                            <View style={styles.indicatorBlock}>

                                {(item.item === 'Earth leak' || item.item === 'Wire insulation resistance') ? null:(
                                    <View style={styles.indicatorBlock}>
                                        <View style={[styles.indicator,styles.green]}></View>
                                        <View style={[styles.indicator,styles.red]}></View>
                                    </View>
                                    ) }
                            </View>


                    </View>
                </View>
            </View>

        )
    }
    render(){
        return(
            <View style={{height:hp(85), flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
                <View style={styles.systemsContainer}>
                    <FlatList
                        vertical
                        numColumns={1}
                        containerStyle={styles.systemsWrap}
                        data={arr}
                        renderItem={this.renderItemList}
                    />
                </View>

                <Modal
                    animationType="fade"
                    transparent
                    visible={this.state.modalInfo}
                    onRequestClose={() => {
                    }}
                    hardwareAccelerated={true}
                >
                    <View style={styles.modalInfoWrap}>
                        <View
                            style={{
                                width: wp(100),
                                height: hp(100),
                                position: "absolute",
                                left: 0,
                                top: 0,
                                backgroundColor: "rgba(255,255,255, .5)"
                            }}
                        />
                        <View style={styles.modalBody}>
                            <Text style={styles.textModal}>Earth leak</Text>
                            <View style={styles.infoTitle}>
                                <Text  style={styles.textModal}>Value:</Text>
                                <Text  style={styles.textModal}>123</Text>
                            </View>

                        </View>
                    </View>
                </Modal>
                {/*<View style={styles.infoBlock}>*/}
                    {/*<View>*/}
                        {/*<Text style={styles.infoText}>Battery </Text>*/}
                    {/*</View>*/}
                    {/*<View>*/}
                        {/*<View style={styles.indicatorBlock}>*/}
                            {/*<View style={[styles.indicator,styles.activeGreen]}></View>*/}
                            {/*<View style={[styles.indicator,styles.activeGreen]}></View>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</View>*/}
                {/*<View style={styles.infoBlock}>*/}
                    {/*<View>*/}
                        {/*<Text style={styles.infoText}>Chain 250V 50Hr </Text>*/}
                    {/*</View>*/}
                    {/*<View>*/}
                        {/*<View style={styles.indicatorBlock}>*/}
                            {/*<View style={[styles.indicator,styles.green]}></View>*/}
                            {/*<View style={[styles.indicator,styles.red]}></View>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</View>*/}
                {/*<View style={styles.infoBlock}>*/}
                    {/*<View>*/}
                        {/*<Text style={styles.infoText}>High Voltage line</Text>*/}
                    {/*</View>*/}
                    {/*<View>*/}
                        {/*<View style={styles.indicatorBlock}>*/}
                            {/*<View style={[styles.indicator,styles.green]}></View>*/}
                            {/*<View style={[styles.indicator,styles.green]}></View>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</View>*/}
                {/*<View style={styles.infoBlock}>*/}
                    {/*<View>*/}
                        {/*<Text style={styles.infoText}>Voltage line</Text>*/}
                    {/*</View>*/}
                    {/*<View>*/}
                        {/*<View style={styles.indicatorBlock}>*/}
                            {/*<View style={[styles.indicator,styles.green]}></View>*/}
                            {/*<View style={[styles.indicator,styles.red]}></View>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</View>*/}
            </View>
        )
    }


}
const arr = [
    'generator','Battery','Chain 250V 50Hr','High Voltage line','Low Voltage line','Earth leak','Wire insulation resistance'
];
const styles = StyleSheet.create({
    infoBlock:{
        width:'85%',
        height: wp(20),
        borderRadius:wp(2),
        backgroundColor:'#FAFAFA',



        shadowColor: 'rgba(0, 0, 0, 0.2)',
        shadowOpacity: 0.2,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        marginBottom:wp(3),
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:25
    },
    indicatorBlock:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        width:wp(11)
    },
    indicator:{
        width:wp(5),
        height:wp(5),
    },
    green:{
        backgroundColor: '#80BD9E'
    },
    red:{
        backgroundColor: '#F98866'
    },

    activeGreen:{
        backgroundColor: '#89DA59'
    },
    activeRed:{
        backgroundColor: '#FF420E'
    },
    infoText:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(4.4),
        // fontFamily:'Lato-Regular',
    },
    systemsWrap: {
        flexDirection: 'column',
        // flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',

    },
    systemsContainer: {
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalInfoWrap: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        height: "100%",
        width: "100%",
        position: "absolute",
        left: 0,
        top: 0
    },
    modalBody:{
        width:wp(80),
        height:wp(25),
        backgroundColor: "#ffffff",
        borderRadius:wp(2),
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "column",
        shadowColor: 'rgba(0, 0, 0, 0.6)',
        shadowOpacity: 0.5,
        shadowRadius: 6,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        paddingVertical: 15

    },
    infoTitle:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        width:'100%',
        paddingHorizontal: 40

    },
    textModal:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(4.4),
        // fontFamily:'Lato-Regular',
    }
})
