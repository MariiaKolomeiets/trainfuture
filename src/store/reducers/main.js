import {
    I_TEMP,
    E_TEMP,
    C_TEMP,
    WIND_SPEED,
    HUMIDITY,
    GENERATOR,
    BRIGHTNESS,
    BOILER,
    BATTERY,
    VOLTAGE,
    CHAIN_250,
    MICROWAVE,
    FRIDGE,
    PLUG,
    WC,
    REAR_LAMPS,
    BRIGHT, TERMOSTATE_MODE
} from "../type";

const initialState = {
    externalTemperature: 12,
    internalTemperature: 24,
    carriageTemperature: 24,
    windSpeed: 30,
    termostateMode:'warm',
    humidity: 50,
    generator: 0,
    battery: -6,
    voltage: 86,
    Chain250: true,
    microwave: true,
    fridge: false,
    plug: true,
    boiler: true,
    WC: true,
    // WC1:true,
    // WC2:false,
    rearLamps: true,
    bright: true,
    brightness: 50
};


const main = (state = initialState, action) => {
    switch (action.type) {
        case E_TEMP:
            return {...state, externalTemperature: action.response};
        case I_TEMP:
            return {...state, internalTemperature: action.response};
        case C_TEMP:
            return {...state, carriageTemperature: action.response};
        case WIND_SPEED:
            return {...state, windSpeed: action.response};
        case HUMIDITY:
            return {...state, humidity: action.response};
        case GENERATOR:
            return {...state, generator: action.response};
        case BATTERY:
            return {...state, battery: action.response};
        case VOLTAGE:
            return {...state, voltage: action.response};
        case CHAIN_250:
            return {...state, voltage: action.response};
        case MICROWAVE:
            return {...state, microwave: action.response};
        case FRIDGE:
            return {...state, fridge: action.response};
        case PLUG:
            return {...state, plug: action.response};
        case BOILER:
            return {...state, boiler: action.response};
        case WC:
            return {...state, WC: action.response};
        case REAR_LAMPS:
            return {...state, rearLamps: action.response};
        case BRIGHT:
            return {...state, bright: action.response};
        case BRIGHTNESS:
            return {...state, brightness: action.response};
        case TERMOSTATE_MODE:
            return {...state, termostateMode: action.response};
        default:
            return state
    }
};
export default main;
