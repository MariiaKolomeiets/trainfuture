import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';
import thunkMiddleware from 'redux-thunk';
import * as reducers from './reducers';

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};

const reducer = combineReducers({...reducers});
const loggerMiddleware = createLogger();

const middlewares = [loggerMiddleware, thunkMiddleware];
const persistedReducer = persistReducer(persistConfig, reducer);

const createStoreWithMiddlewares = applyMiddleware(...middlewares)(createStore);

const store = createStoreWithMiddlewares(persistedReducer);

const  persistor = persistStore(store);

export { store, persistor }
