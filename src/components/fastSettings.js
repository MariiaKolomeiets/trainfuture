import React, {Component, Fragment} from 'react';
import {StyleSheet, Text, View, Image, Switch} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"


export const FastSettings = (props) => {


    return (
        <View style={styles.blockWrap}>
            <View style={[styles.modeWrap, {paddingBottom:10}]}>
                <Text style={styles.textMode}>Power Off</Text>
                <View style={styles.switchWrap}>
                    <Switch value={true} style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }] }}   trackColor={{true: '#ffc580', false: '#ffc580'}}  />
                </View>

            </View>
            <View style={styles.modeWrap}>
                <Text style={styles.textMode}>Automatic Mode On</Text>
                <View style={styles.switchWrap}>
                <Switch value={false} style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }] }}    trackColor={{true: '#ffc580', false: '#ffc580'}}  />
                </View>
            </View>
        </View>
    )
};
const styles = StyleSheet.create({
    blockWrap:{
        width:'100%',
        height:wp(30),
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:20,
        paddingVertical:20

    },
    textMode:{
        fontFamily: 'Oswald-Regular',
        fontSize: wp(5.4),
        color: '#373027',
        flex:1,
    },
    switchWrap:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-end',
    },
    modeWrap:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    }
})
