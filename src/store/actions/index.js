import {
    BOILER,
    C_TEMP,
    FRIDGE,
    HUMIDITY,
    MICROWAVE,
    PLUG,
    REAR_LAMPS,
    SET_USER,
    TERMOSTATE_MODE,
    WC,
    WIND_SPEED
} from "../type";

export const setCarriageTemp = response => {
  return {
    type: C_TEMP,
      response
  };
};

export const setWindSpeed = response => {
    return {
        type: WIND_SPEED,
        response
    };
};

export const setHumidity = response => {
    return {
        type: HUMIDITY,
        response
    }
}
export const changeStateMicro = response => {
    return {
        type: MICROWAVE,
        response
    }
};

export const changeStateFridge = response =>{
    return {
        type: FRIDGE,
        response
    }
};
export  const  changeStatePlug = response =>{
    return {
        type: PLUG,
        response
    }
};

export const changeStateBoiler = response =>{
    return {
        type: BOILER,
        response
    }
};
export const changeStateWC = response => {
    return {
        type: WC,
        response
    }
};
export const changeStateRearLamps = response =>{
    return {
        type: REAR_LAMPS,
        response
    }
};

export const changeTermostateMode = response => {
    return{
        type: TERMOSTATE_MODE,
        response
    }
}


/* ======== UserActions ====== */

export const setUser = response => {
    return {
        type: SET_USER,
        response
    }
}
