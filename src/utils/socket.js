import io from 'socket.io-client';
let socket;

const connect = () => {
    socket = io('http://192.168.46.201:8080');
}

const emit = (chanel, message) => {
    if(socket){
        socket.emit(chanel,message);
    }
};

const listen = () => {
   socket.on("test", (val)=>{
       console.log('IN', val)
   })
}

const socketEmmiter = (type, cb) => {
  socket.on(type, cb)
}

export const SocketUtils = {
    connect,
    emit,
    listen,
    socketEmmiter,
};
