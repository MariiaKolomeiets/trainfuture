import React, {Component, Fragment} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Thermometer from './Thermometer'
import Humidity from "./Humidity";

const tabs = [Thermometer, Humidity];

export default class Thermostate  extends Component{

  state={
    activeTab:0
  }


  tabStyle = index => {
    const { activeTab } = this.state;
    const activeStyle = index === activeTab ? { borderBottomWidth: 2,
      borderBottomColor: '#F6C78A' } : {};
    return [styles.tabHeader, activeStyle]
  }

  setCurrentTab = index => this.setState({ activeTab: index })


    render(){
    const { activeTab } = this.state;
      const Component = tabs[activeTab]


        return(
          <View style={styles.page}>
            <View style={styles.tabsContainer}>
              <TouchableOpacity style={this.tabStyle(0)} onPress={() => this.setCurrentTab(0)}>
                <Text>Temperature</Text>
              </TouchableOpacity>
              <TouchableOpacity style={this.tabStyle(1)} onPress={() => this.setCurrentTab(1)}>
                <Text>Humidity</Text>
              </TouchableOpacity>
            </View>
            <View>
              <Component />
            </View>
          </View>
        )
    }


}
const styles = StyleSheet.create({
  page: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginTop: -25,
  },
  tabsContainer: {
    width: '100%',
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  tabHeader: {
    flex: 1,
    height: '100%',
    width: '50%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',

  },
  tabContainer: {

  }
});

