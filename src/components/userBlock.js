import React, {Component, Fragment} from 'react';
import {StyleSheet, Text, View, Image} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"

import LinearGradient from 'react-native-linear-gradient';

export const UserBlock = (props) => {



    return (
        <View style={styles.blockWrap}>
            <LinearGradient colors={['#ffdcb3', '#ffd19a','#ffc580']} style={styles.linearGradient}>
                <View style={styles.settingsWrap}>
                    <Text style={styles.title}>Settings</Text>
                    <View style={styles.editContainer}>
                        <View style={styles.avatarWrap}>
                        <View>
                            <View style={styles.avatar}>
                                <Image style={styles.avatar} source={require('../../assets/test.jpg')}/>
                            </View>
                        </View>
                        <View style={styles.userInfoWrap}>
                            <Text style={styles.hello}>Hello</Text>
                            <Text style={styles.userName}>Kolomeiets Mariia</Text>
                        </View>
                        </View>
                        <View style={styles.editBtn}>
                            <Image  style={styles.editIcon} source={require('../../assets/icons/settings/edit.png')}/>
                        </View>
                    </View>

                </View>
            </LinearGradient>
        </View>
    )
};
const styles = StyleSheet.create({
    blockWrap:{
        width:'100%',
        height:wp(50)
    },
    linearGradient:{
        width: '100%',
        height: '100%'
    },
    title:{
        fontFamily: 'Oswald-Regular',
        fontSize: wp(6.4),
        color: '#373027',
        paddingVertical:20
    },
    settingsWrap:{
        width: '100%',
        height: '100%',
        paddingHorizontal:20
    },
    avatar:{
        height:85,
        width:85,
        backgroundColor:'#d3d3d3',
        borderRadius:85
    },
    avatarWrap:{
        flex:9,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-around'
    },
    editBtn:{
        flex:3,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-end'
    },
    editIcon:{
        height:wp(6),
        width:wp(6)
    },
    editContainer:{
        width: '100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    hello:{
        fontFamily: 'Lato-Regular',
        fontSize: wp(4),
        color: '#373027',
        textAlign:'center'
    },
    userName:{
        fontFamily: 'Lato-Bold',
        fontSize: wp(4.4),
        color: '#373027',
        textAlign:'center'
    },
    userInfoWrap:{
        flexDirection:'column',
        alignItems:'flex-start',
        justifyContent:'space-between'
    }
})
