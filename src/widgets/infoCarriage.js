import React, {Component, Fragment} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

export const InfoCarriage = (props) => {
    const {c_temp, e_temp, humidity} = props;
    return (
        <View style={styles.container}>
            <View style={styles.lineInfo}>
                <View style={styles.info}>
                    <Text style={styles.number}>{c_temp} ℃</Text>
                    <Text style={styles.text}>avg. carriage temp</Text>
                </View>
                <View style={styles.verticalDivider}></View>
                <View style={styles.info}>
                    <Text style={styles.number}>{humidity} %</Text>
                    <Text style={styles.text}>avg. humidity</Text>
                </View>
            </View>
            <View style={styles.divider}></View>
            <View style={styles.lineInfo}>
                <View style={styles.info}>
                    <Text style={styles.number}>{e_temp} ℃</Text>
                    <Text style={styles.text}>avg. outside temp</Text>
                </View>
                <View style={styles.verticalDivider}></View>
                <View style={styles.info}>
                    <Text style={styles.number}>0</Text>
                    <Text style={styles.text}>errors</Text>
                </View>
            </View>
        </View>
    )
};
export const InfoPower = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.lineInfo}>
                <View style={styles.info}>
                    <Text style={styles.number}>0 A</Text>
                    <Text style={styles.text}>generator amp.</Text>
                </View>
                <View style={styles.verticalDivider}></View>
                <View style={styles.info}>
                    <Text style={styles.number}>86 V</Text>
                    <Text style={styles.text}>voltage</Text>
                </View>
            </View>
            <View style={styles.divider}></View>
            <View style={styles.lineInfo}>
                <View style={styles.info}>
                    <Text style={styles.number}>-6 A</Text>
                    <Text style={styles.text}>battery amp.</Text>
                </View>
                <View style={styles.verticalDivider}></View>
                <View style={styles.info}>
                    <Text style={styles.number}>0</Text>
                    <Text style={styles.text}>errors</Text>
                </View>
            </View>
        </View>
    )
};
export const InfoSections = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.sectionsContainer}>
                <View style={styles.vestibule}>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                        {/*<View style={styles.wrapVest}>*/}
                            {/*<View  style={styles.indicatorWrap}>*/}
                                {/*<View style={[styles.indicator]}></View>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                        {/*<View style={styles.wrapVest}>*/}
                            {/*<View  style={styles.indicatorWrap}>*/}
                                {/*<View style={[styles.indicator]}></View>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                        {/*<View style={styles.wrapVest}>*/}
                            {/*<View  style={styles.indicatorWrap}>*/}
                                {/*<View style={[styles.indicator]}></View>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                        {/*<View style={styles.wrapVest}>*/}
                            {/*<View  style={styles.indicatorWrap}>*/}
                                {/*<View style={[styles.indicator]}></View>*/}
                            {/*</View>*/}
                        {/*</View>*/}
                    </View>
                </View>
                <View style={styles.peopleSections}>
                    <View style={styles.sectionWrap}>
                        <View style={[styles.section, styles.firstSection]}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>1</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View  style={styles.indicatorWrap}>
                                <View style={[styles.indicator]}></View>
                                <Text style={styles.sectionText}>2</Text>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>3</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>4</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>5</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>6</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>7</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>8</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.section}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>9</Text>
                                </View>
                            </View>
                        </View>
                        <View style={[styles.section]}>
                            <View style={styles.section}>
                                <View  style={styles.indicatorWrap}>
                                    <View style={[styles.indicator]}></View>
                                    <Text style={styles.sectionText}>10</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.coridorWrap}></View>
                </View>
                <View style={styles.vestibule}>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.block}>
                        <View style={[styles.indicator]}></View>
                    </View>
                </View>
            </View>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        borderRadius: wp(7),
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        shadowOpacity: 0.2,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },
    },
    divider: {
        width: '100%',
        height: 1,
        backgroundColor: '#e7e7e6'
    },
    verticalDivider: {
        height: '100%',
        width: 1,
        backgroundColor: '#e7e7e6'
    },
    lineInfo: {
        flexDirection: 'row',
        flex: 1
    },
    info: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 5
    },
    number: {
        fontSize: wp(4.4),
        fontFamily: 'Oswald-Bold',
        color: '#ffc580'
    },
    text: {
        fontSize: wp(3),
        fontFamily: 'Lato',
        color: '#373027',
    },
    sectionsContainer: {
        height: '95%',
        width: '100%',
        flexDirection: 'row'
    },
    vestibule: {
        flex: 2,
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'space-around'

    },
    section: {
        flex: 1,
        borderColor: '#e7e7e6',
        borderLeftWidth: 1,

        paddingHorizontal: 2,
        flexDirection:'row',
        alignItems:'flex-start',
        justifyContent:'center'
    },
    firstSection:{
        borderLeftWidth: 0,
    },
    peopleSections: {
        flex: 10,
        borderColor: '#e7e7e6',
        borderLeftWidth: 1,
        borderRightWidth: 1,
    },
    sectionWrap: {
        flexDirection:'row',
        height:'70%',
        flex: 8.5,
        borderColor:'#e7e7e6',
        borderBottomWidth:1
    },
    coridorWrap:{
        height:'35%',
        flex: 3.5
    },
    indicator:{
        width:wp(2),
        height:wp(2),
        borderRadius: wp(2),
        backgroundColor:'#88DAAE'
    },
    sectionText:{
        fontSize: wp(3),
        fontFamily: 'Lato',
        color: '#373027',
        paddingTop:wp(1)
    },
    indicatorWrap:{
        paddingTop: wp(5)
    },
    block:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    }
})
