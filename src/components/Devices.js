import React, {Component, Fragment} from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback, FlatList, Switch, Modal} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
    changeStateBoiler,
    changeStateFridge,
    changeStateMicro,
    changeStatePlug,
    changeStateRearLamps,
    changeStateWC
} from "../store/actions";

 class Devices extends Component {
    constructor(props) {
        super(props);
        const {microwave,fridge, plug,boiler,WC,rearLamps} = props;
        this.state = {
            microwave:microwave,
            fridge:fridge,
            plug:plug,
            boiler:boiler,
            WC:WC,
            rearLamps:rearLamps,
            modalInfo: false
        };
    }

    changeMode = (device,state, changeMode) =>{
        this.setState({[device]: state});

        this.props[changeMode](state);
    };



    renderItem = ({item, index}) => {
        const state = this.state[item.state];
        return (
            <View style={styles.device}>
                <Text style={styles.deviceText}>{item.name}</Text>
                <Switch value={state} style={{ transform: [{ scaleX: .8 }, { scaleY: .8 }] }}
                        onValueChange={(value)=>{this.changeMode(item.state, value, item.changeMode)}}
                        trackColor={{true: '#ffc580', false: '#e8e8e8'}}  />
                {/*<Text style={item.error ? styles.error : styles.ok}>{item.error ? '✘': '✔'}</Text>*/}
                <Image style={{...item.sizes}} source={item.icon}/>
            </View>
        );
    };

    render() {
        return (
            <View style={{
                height: hp(90),
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-between',
            }}>
                <View style={styles.divicesWrap}>
                    <FlatList data={DeviceArr}
                              numColumns={2}
                              keyExtractor={(item, index) => index.toString()}
                              renderItem={this.renderItem}
                    />
                </View>
                <Modal
                    animationType="fade"
                    transparent
                    visible={this.state.modalInfo}
                    onRequestClose={() => {
                    }}
                    hardwareAccelerated={true}
                >
                    <View style={styles.modalInfoWrap}>
                        <View
                            style={{
                                width: wp(100),
                                height: hp(100),
                                position: "absolute",
                                left: 0,
                                top: 0,
                                backgroundColor: "rgba(255,255,255, .5)"
                            }}
                        />
                        <View style={styles.modalBody}>
                            <View style={styles.infoTitle}>
                                <Text style={styles.infoState}>State</Text>
                                <Text style={styles.error}>✘</Text>
                            </View>
                            <Text> Please check the power supply.</Text>
                            <View style={styles.infoTitle}>
                                <Text style={styles.infoState}>Terminal Voltage:</Text>
                                <Text style={styles.error}> 90V</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }


}
const putActionsToProps = dispatch => {
    return {
        changeStateBoiler: bindActionCreators(changeStateBoiler, dispatch),
        changeStateFridge: bindActionCreators(changeStateFridge, dispatch),
        changeStateMicro: bindActionCreators(changeStateMicro, dispatch),
        changeStatePlug: bindActionCreators(changeStatePlug,dispatch),
        changeStateRearLamps: bindActionCreators(changeStateRearLamps, dispatch),
        changeStateWC: bindActionCreators(changeStateWC, dispatch)
    };
};
export default connect(
    state =>({
        microwave: state.main.microwave,
        fridge:state.main.fridge,
        plug: state.main.plug,
        boiler: state.main.boiler,
        WC: state.main.boiler,
        rearLamps:state.main.rearLamps

    }),
    putActionsToProps
)(Devices)

const styles = StyleSheet.create({
    divicesWrap: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: wp(5),
    },
    device: {
        width: wp(40),
        height: wp(40),
        backgroundColor: '#fff',
        borderRadius: wp(1),
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        shadowOpacity: 0.6,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        flex: 1,
         margin: wp(2.5)
    },
    deviceIcon:{
        width:wp(12),
        height:wp(9)
    },
    deviceText:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(4),
        color: '#373027',
        textAlign:'center'
    },
    error:{
        color:'#fc5047',
        fontFamily:'Oswald-Regular',
        fontSize:wp(4),
    },
    ok:{
        color:'#34fab1'
    },
    modalInfoWrap: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        height: "100%",
        width: "100%",
        position: "absolute",
        left: 0,
        top: 0
    },
    modalBody:{
        width:wp(80),
        height:wp(25),
        backgroundColor: "#ffffff",
        borderRadius:wp(2),
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "column",
        shadowColor: 'rgba(0, 0, 0, 0.6)',
        shadowOpacity: 0.5,
        shadowRadius: 6,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        paddingVertical: 15

    },
    infoTitle:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        width:'100%',
        paddingHorizontal: 40

    },
    infoState:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(4),
        color: '#373027',
        textAlign:'center'
    }
});


const DeviceArr = [
    {
        name: 'Microwave',
        icon_active: require('../../assets/icons/devices/microwave-active.png'),
        icon: require('../../assets/icons/devices/microwave.png'),
        sizes: {
            width: wp(12),
            height: wp(9),
        },
        state:'microwave',
        changeMode:'changeStateMicro',
        error:true
    },
    {
        name: 'Fridge',
        icon_active: require('../../assets/icons/devices/fridge-active.png'),
        icon: require('../../assets/icons/devices/fridge.png'),
        sizes: {
            width: wp(7),
            height: wp(12),
        },
        state:'fridge',
        changeMode:'changeStateFridge',
        error:false
    },
    {
        name: 'Plug',
        icon_active: require('../../assets/icons/devices/plug-active.png'),
        icon: require('../../assets/icons/devices/plug.png'),
        sizes: {
            width: wp(9),
            height: wp(11),
        },
        state:'plug',
        changeMode:'changeStatePlug',
        error:true
    },
    {
        name: 'Boiler',
        icon_active: require('../../assets/icons/devices/water_heater-active.png'),
        icon: require('../../assets/icons/devices/water_heater.png'),
        sizes: {
            width: wp(9),
            height: wp(12),
        },
        state:'boiler',
        changeMode:'changeStateBoiler',
        error:false
    },
    {
        name: 'WC',
        icon_active: require('../../assets/icons/devices/wc-active.png'),
        icon: require('../../assets/icons/devices/wc.png'),
        sizes: {
            width: wp(12),
            height: wp(10),
        },
        state:'WC',
        changeMode:'changeStateWC',
        error:true
    },
    {
        name: 'Carriage Rear Lamps',
        icon_active: require('../../assets/icons/devices/lamp-active.png'),
        icon: require('../../assets/icons/devices/lamp.png'),
        sizes: {
            width: wp(9),
            height: wp(9),
        },
        state:'rearLamps',
        changeMode:'changeStateRearLamps',
        error:true
    },
];


