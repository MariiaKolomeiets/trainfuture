import {SECTIONS, SET_USER} from "../type";

const initialState = {
    accessToken: null,
    email:null,
    userId: null

};

const auth = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                ...action.response,
            };
        default:
            return state;
    }
};

export default auth;
