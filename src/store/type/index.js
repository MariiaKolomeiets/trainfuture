export const I_TEMP = 'I_TEMP';
export const E_TEMP = 'E_TEMP';
export const C_TEMP = 'C_TEMP';
export const WIND_SPEED = 'WIND_SPEED';
export const HUMIDITY='HUMIDITY';
export const GENERATOR ='GENERATOR';
export const BATTERY ='BATTERY';
export const VOLTAGE ='VOLTAGE';
export const CHAIN_250 ='CHAIN_250';
export const MICROWAVE = 'MICROWAVE';
export const FRIDGE = 'FRIDGE';
export const PLUG = 'PLUG';
export const BOILER = 'Boiler';
export const WC = 'WC';
export const REAR_LAMPS = 'REAR_LAMPS';
export const BRIGHT = 'BRIGHT';
export const BRIGHTNESS = 'BRIGHTNESS';
export const SECTIONS = 'SECTIONS';
export const TERMOSTATE_MODE = 'TERMOSTATE_MODE';
export const SET_USER = 'SET_USER';
