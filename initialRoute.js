import React, {Component} from 'react';
import {
    View,
} from 'react-native';
import {connect} from "react-redux";

class InitialRoute extends Component {

    componentDidMount() {
        const user = {
            name: 'Maria',
            userId: null
        };
        this._bootstrapAsync(user);
    }

    _bootstrapAsync = () => {
        if (this.props.accessToken) {
            this.props.navigation.navigate('App');
        } else {

            this.props.navigation.navigate('Start');
        }
    };

    render() {
        return (
            <View/>
        );
    }
}

export default connect(
    state => ({
        accessToken: state.auth.accessToken,

    })
)(InitialRoute)
