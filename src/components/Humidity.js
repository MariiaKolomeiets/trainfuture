import React, {Component, Fragment} from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import { Picker } from 'react-native-wheel-datepicker';
import CircularSlider from './CircleSlider';
import Slider from '@react-native-community/slider';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {setHumidity} from "../store/actions";

 class Humidity  extends Component{
    constructor(props) {
        super(props);
        const {humidity} = props;
        this.state = {
            humidity: humidity,
            choosenIndex:5,
            pickerValues: []

        };
    }

    componentWillMount() {
      const pickerValues = []
      for(let i = 50; i < 61; i++) {
        pickerValues.push({ value: i, label: `${i}%` })
      }
      this.setState({pickerValues})
    }

    changeValue = (itemValue,itemPosition) => {
        const {setHumidity} = this.props;
        this.setState({humidity: itemValue, choosenIndex: itemPosition});
        setHumidity(itemValue);

    };


    render(){
        const {humidity, pickerValues} = this.state;
        return(
            <View style={{height:hp(70), flexDirection:'column', alignItems:'center', justifyContent:'space-around'}}>
                {/*<Picker style={styles.pickerStyle}
                        selectedValue={humidity}
                        onValueChange={(itemValue, itemPosition) =>{this.changeValue(itemValue, itemPosition)}}
                >
                    <Picker.Item label="50%" value="50" />
                    <Picker.Item label="51%" value="51" />
                    <Picker.Item label="52%" value="52" />
                    <Picker.Item label="53%" value="53" />
                    <Picker.Item label="54%" value="54" />
                    <Picker.Item label="55%" value="55" />
                    <Picker.Item label="56%" value="56" />
                    <Picker.Item label="57%" value="57" />
                    <Picker.Item label="58%" value="58" />
                    <Picker.Item label="59%" value="59" />
                    <Picker.Item label="60%" value="60" />
                </Picker>*/}
              <View style={styles.pickerContainer}>
                <Picker
                  style={styles.picker}
                  pickerData={pickerValues}
                  value={humidity}
                  selectedValue={humidity}
                  onValueChange={value => this.changeValue(value)}
                />
              </View>
                <View style={styles.aboutHumidityWrap}>
                   <Text style={styles.aboutHumidity}>
                       Maintaining relative humidity below <Text style={styles.bold}>50%</Text> prevents dust mite infestations, mold and mildew growth, and inhibits bacteria. This lower relative humidity also reduces the out-gassing of VOCs. In colder climates, wintertime humidity levels must be even lower — generally <Text style={styles.bold}>30–40%</Text> — to prevent condensation on windows and other surfaces.
                   </Text>
                </View>
            </View>
        )
    }


}
const styles = StyleSheet.create({
    sliderWrap:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        width:'70%'
    },
    aboutHumidity:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(4),
        paddingHorizontal:wp(7),
        textAlign:'center',
        color:'#373027'
    },
    bold:{
        fontFamily:'Oswald-Bold',
        color:'#ffc580',
        fontSize:wp(5),
    },
    aboutHumidityWrap:{},
    pickerStyle:{
        height: 150,
        width: "80%",
        color: '#344953',
        justifyContent: 'center',
    },
  pickerContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: '85%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  picker: { flex: 1, height: 150, backgroundColor: '#fff' }
})
const putActionsToProps = dispatch => {
    return {
        setHumidity: bindActionCreators(setHumidity, dispatch),
    };
};
export default connect(
    state =>({
        humidity:state.main.humidity
    }),
    putActionsToProps
)(Humidity)
