import React, {Component, Fragment} from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback, TouchableOpacity, Modal} from 'react-native';
import {NotificationsErrors} from '../components/notificationErrors';
import {NotificationsWarnings} from '../components/NotificationsWarnings';
import {Requests} from "../components/Requests";
import {heightPercentageToDP as  hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Button, Container} from "native-base";
import { SocketUtils } from '../utils/socket';

import ApiService from '../utils/api'

const tabs = [NotificationsErrors, NotificationsWarnings, Requests];

export default class NotificationScreen extends Component {
    state = {
        activeTab:0,
        modalRequest: false,
      errors: [],
    };


    tabStyle = index => {
        const { activeTab } = this.state;
        const activeStyle = index === activeTab ? { borderBottomWidth: 2,
            borderBottomColor: '#F6C78A' } : {};
        return [styles.tabHeader, activeStyle]
    }

    componentDidMount() {
      SocketUtils.socketEmmiter('emmit_check_errors', this.checkErrors);
      this.checkErrors();
    }

    checkErrors = async () => {
      const errors = await ApiService.get('/carriage/check_errors');
      this.setState({ errors: errors.errors })
      console.log(errors)
    }

    setCurrentTab = index => this.setState({ activeTab: index });

    closeReqModal = () =>  this.setState({ modalRequest: false });

    render() {
        const { activeTab, errors } = this.state;
        const Component = tabs[activeTab];
        return (
            <>
                <View style={styles.page}>
                    <View style={styles.tabsContainer}>
                        <TouchableOpacity style={this.tabStyle(0)} onPress={() => this.setCurrentTab(0)}>
                            <Text style={styles.tabText}>Errors</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.tabStyle(1)} onPress={() => this.setCurrentTab(1)}>
                            <Text style={styles.tabText}>Warnings</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={this.tabStyle(2)} onPress={() => this.setCurrentTab(2)}>
                            <Text style={styles.tabText}>Requests</Text>
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Component errors={errors} />
                    </View>
                </View>
                <Modal
                    animationType="fade"
                    transparent
                    visible={this.state.modalRequest}
                    onRequestClose={() => {
                    }}
                    hardwareAccelerated={true}
                >
                    <TouchableWithoutFeedback onPress={this.closeReqModal}>
                    <View style={styles.modalInfoWrap}>
                        <View
                            style={{
                                width: wp(100),
                                height: hp(100),
                                position: "absolute",
                                left: 0,
                                top: 0,
                                backgroundColor: "rgba(255,255,255, .5)"
                            }}
                        />
                       <View style={styles.requestWrap}>
                           <Text style={styles.textReqTitle}>Request</Text>
                           <View style={styles.requestBody}>
                            <View style={styles.tableLine}>
                                <Text style={styles.firstOne}> Train:</Text>
                                <View style={styles.divider}></View>
                                <Text style={styles.secondOne}> 624A</Text>
                            </View>
                               <View style={styles.tableLine}>
                                   <Text style={styles.firstOne}> Carriage:</Text>
                                   <View style={styles.divider}></View>
                                   <Text style={styles.secondOne}> 12</Text>
                               </View>
                               <View style={styles.tableLine}>
                                   <Text style={styles.firstOne}> Problem:</Text>
                                   <View style={styles.divider}></View>
                                   <Text style={styles.secondOne}> Error: Section 2. Ventilation error.</Text>
                               </View>
                               <View style={[styles.tableLine, styles.lastLine]}>
                                   <Text style={styles.firstOne}> Steward:</Text>
                                   <View style={styles.divider}></View>
                                   <Text style={styles.secondOne}> Kolomeiets M.</Text>
                               </View>
                           </View>
                           <TouchableWithoutFeedback >
                               <View style={styles.sendBtn}>
                               <Text style={styles.sendText}> To work</Text>
                               </View>
                           </TouchableWithoutFeedback>
                       </View>
                    </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </>
        );
    }
}
const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    tabsContainer: {
        width: '100%',
        flexDirection: 'row',
        height: 40,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    tabHeader: {
        flex: 1,
        height: '100%',
        width: '50%',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',

    },
    tabText:{
        fontFamily: 'Lato-Bold',
        fontSize: wp(4.4),
        color: '#373027',
    },
    modalInfoWrap: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        height: "100%",
        width: "100%",
        position: "absolute",
        left: 0,
        top: 0
    },
    requestWrap:{
        width:wp(80),
        backgroundColor: "#ffffff",
        borderRadius:wp(2),
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        shadowColor: 'rgba(0, 0, 0, 0.6)',
        shadowOpacity: 0.5,
        shadowRadius: 6,
        shadowOffset: {
            width: 1,
            height: 4,
        },

    },
    textReqTitle:{
        fontFamily: 'Lato',
        fontSize: wp(5.4),
        color: '#373027',
        textAlign:'center',
        paddingVertical:20,
        paddingHorizontal:30
    },
    requestBody:{
        width:'80%',
        borderWidth:1,
        borderColor:'#373027',
    },
    tableLine:{
        width:'100%',
        borderBottomColor: '#373027',
        borderBottomWidth: 1,
        justifyContent: "space-between",
        alignItems: "center",
        flexDirection: "row",
    },
    lastLine:{
        borderBottomColor:'transparent'
    },
    divider:{
        height:'100%',
        width:1,
        backgroundColor:'#373027',
    },
    firstOne:{
        flex:5,
        // fontFamily: 'Lato-Bold',
        fontFamily: 'Oswald-Regular',
        fontSize: wp(4),
        color: '#373027',
        textAlign:'center'
    },
    secondOne:{
        flex:7,
        fontFamily: 'Oswald-Bold',
        fontSize: wp(4),
        color: '#373027',
        textAlign:'center'
    },
    sendBtn:{
        width:'50%',
        height:wp(10),
        backgroundColor:'#ffc580',
        borderRadius:wp(2),
        marginVertical: 30,
        marginHorizontal: 10,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    sendText:{
        fontFamily: 'Lato',
        fontSize: wp(5.4),
        color: '#373027',
        textAlign:'center',
    }
})

