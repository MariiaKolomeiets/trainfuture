import {SECTIONS} from "../type";

const initialState = {
  sections: [],

};

const carriage = (state = initialState, action) => {
  switch (action.type) {
    case SECTIONS:
      return {
        ...state,
        sections: action.results,
      };
    default:
      return state;
  }
};

export default carriage;
