import React, {Component, Fragment} from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback, Switch, Modal, TouchableHighlight} from 'react-native';
import {SocketUtils} from '../utils/socket';
import BigSlider from '../widgets/BigSlider';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DateTimePicker from "react-native-modal-datetime-picker";

export default class Brightness extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bright: 50,
            valB: 50,
            safeMode: false,
            isDateTimePickerVisible: false,
            time: [
                {from: '17:30',to: '22.30'}
            ],
            editField:'from'
        };
    }

    changeBright = (value) => {
        SocketUtils.emit("change_brightness", value);
        this.setState({bright: value});
    };

    switch = () => {
        const {safeMode} = this.state;
        this.setState({safeMode: !safeMode})
    };
    showDateTimePicker = (editField) => {
        this.setState({isDateTimePickerVisible: true,editField });
    };

    hideDateTimePicker = () => {
        this.setState({isDateTimePickerVisible: false});
    };

    handleDatePicked = date => {
        const {editField, time} = this.state;

        const editArr = time;
        const lastItem = time[time.length-1];
        const selectedTime =  new Date(date);

        lastItem[editField]= `${selectedTime.getHours()}:${selectedTime.getMinutes()}`;
         editArr.splice(time.length-1, lastItem);


        this.setState({time: editArr});
        this.hideDateTimePicker();
    };
    addWorkingTime = () => {
        const {time} = this.state;
        const addTime = [...time,{from:null, to:null}];

        this.setState({time: addTime});

    };

    render() {
        const {bright} = this.state;
        return (
            <View style={{
                height: '100%',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-between',
            }}>
                <View style={styles.brightDetails}>
                    <View style={styles.workDetails}>
                        <Text style={styles.brightText}>Lightening</Text>
                        <Text style={styles.brightNumber}>{(bright * 0.394).toFixed(0)} %</Text>
                        <View>
                            <Text style={styles.saveMode}>Power Save Mode</Text>
                            <Switch value={this.state.safeMode}
                                    style={{transform: [{scaleX: .8}, {scaleY: .8}]}}
                                    onValueChange={this.switch}
                                    trackColor={{true: '#ffc580', false: '#dedede'}}/>
                        </View>
                    </View>
                    <View style={styles.brightSetting}>
                        <View style={styles.sliderWrap}>
                            <BigSlider
                                style={{width: 80}}
                                trackStyle={{backgroundColor: 'rgba(255,197,128,0.7)'}}
                                maximumValue={255}
                                minimumValue={0}
                                onValueChange={this.changeBright}
                                value={this.state.valB}/>
                        </View>
                    </View>

                </View>
                <View style={styles.modeSection}>
                    <Text style={[styles.modeText]}>Daily</Text>
                    <Text style={[styles.modeText, styles.activeMode]}>Night</Text>
                    <Text style={[styles.modeText]}>Service</Text>
                </View>
                <View style={styles.workingHours}>
                    <View style={styles.addTime}>
                        <Text style={styles.addText}>Set Working Hours</Text>
                        <TouchableWithoutFeedback onPress={this.addWorkingTime}>
                        <Text style={styles.add}>+</Text>
                        </TouchableWithoutFeedback>
                    </View>
                    {this.state.time.map(time =>{
                        return (
                            <View style={styles.rangeTimes}>

                                <View style={styles.timeWrap}>
                                    <Text style={styles.textForTime}>
                                        From
                                    </Text>
                                    {time.from ? (
                                        <Text style={styles.time}>
                                            {time.from}
                                        </Text>
                                        ):(
                                        <TouchableWithoutFeedback onPress={()=>this.showDateTimePicker('from')}>
                                            <Text style={styles.time}>
                                                ____
                                            </Text>
                                        </TouchableWithoutFeedback>
                                    )}
                                    <Text style={styles.textForTime}>
                                        To
                                    </Text>
                                    {time.to ? (
                                        <Text style={styles.time}>
                                            {time.to}
                                        </Text>
                                    ):(
                                        <TouchableWithoutFeedback onPress={()=>this.showDateTimePicker('to')}>
                                            <Text style={styles.time}>
                                                ____
                                            </Text>
                                        </TouchableWithoutFeedback>
                                    )}
                                </View>
                                <Text style={styles.delete}>✗</Text>
                            </View>
                        )
                    })}

                </View>
                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    mode={'time'}
                    is24Hour={true}
                />
            </View>
        );
    }


}
const styles = StyleSheet.create({
        sliderWrap: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
            width: 70

        },
        brightDetails: {
            width: '100%',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'flex-end',
            height: '50%',
            paddingHorizontal: 20,
            flex: 4
        },
        workDetails: {
            flex: 1,
            height: '100%',
            flexDirection: 'column',
            alignItems: 'flex-start',
            justifyContent: 'space-between',

        },
        brightSetting: {
            flex: 1,
            height: '100%',
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'center',
        },
        brightText: {
            fontFamily: 'Lato-Bold',
            // fontFamily: 'Oswald-Regular',
            fontSize: wp(5.4),
            color: '#373027',
            marginBottom: wp(10)
        },
        brightNumber: {
            fontFamily: 'Oswald-Regular',
            fontSize: wp(6.4),
            color: '#ffc580',
            marginBottom: wp(10)
        },
        saveMode: {
            fontFamily: 'Lato-Bold',
            fontSize: wp(4),
            color: '#373027',
            marginBottom: wp(2)
        },
        workingHours: {
            width: '100%',
            paddingHorizontal: 20,
            flex: 5
        },

        rangeTimes: {
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingVertical: 7
        },
        time: {
            fontFamily: 'Oswald-Regular',
            fontSize: wp(4),
            color: '#ffc580',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginRight: wp(3)

        },
        timeWrap: {
            flex: 8,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
        },
        delete: {
            fontFamily: 'Lato-Bold',
            fontSize: wp(7),
            color: '#373027',
            flex: 4,
            textAlign: 'right'
        },
        textForTime: {
            fontFamily: 'Lato-Bold',
            fontSize: wp(4.4),
            color: '#373027',
            marginRight: wp(3)
        },
        addText: {
            fontFamily: 'Lato-Bold',
            fontSize: wp(6.4),
            color: '#373027',

        },
        addTime: {
            width: '100%',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginBottom: wp(4)
        },
        add: {
            fontSize: wp(7),
            color: '#bababa'
        },
        modeSection: {
            width: '100%',
            flex: 2,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingHorizontal: 40
        },
        modeText: {
            fontFamily: 'Oswald-Regular',
            fontSize: wp(5),
            color: '#373027',
        },
        activeMode: {
            color: '#ffc580',
        },
        modalInfoWrap: {
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            height: "100%",
            width: "100%",
            position: "absolute",
            left: 0,
            top: 0
        },
        timeModalWrap: {
            width: wp(80),
            height: wp(80),
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: "column",
            backgroundColor: '#ffffff',
            borderRadius: wp(2),
            shadowColor: 'rgba(0, 0, 0, 0.6)',
            shadowOpacity: 0.6,
            shadowRadius: 8,
            shadowOffset: {
                width: 1,
                height: 4,
            },
        },
        timeAddTextTitle: {
            fontFamily: 'Lato',
            fontSize: wp(5.4),
            color: '#373027',
            textAlign: 'center',
            paddingHorizontal: 30,
            paddingVertical: 20,
            borderRadius: wp(2),
            shadowColor: 'rgba(0, 0, 0, 0.4)',
            shadowOpacity: 0.4,
            shadowRadius: 5,
            shadowOffset: {
                width: 1,
                height: 4,
            },
        },
        timeBtn: {
            width: wp(40),
            height: wp(15),
            backgroundColor: '#ffc580',
            borderRadius: wp(2),
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "row",
            marginVertical: 7

        },
        timeText: {
            fontFamily: 'Oswald-Regular',
            fontSize: wp(5),
            color: '#373027',
        },
        btnWrap: {
            width: '100%',
            paddingHorizontal: 30,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",

        },
        saveTimeText: {
            fontFamily: 'Lato',
            fontSize: wp(5.4),
            color: '#373027',
            textAlign: 'center',
        }
    }
);
