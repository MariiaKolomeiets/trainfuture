import {store} from '../../src/store';
import config from '../config'
class ApiService {

  get (url)  {
    return this.request(url, 'GET');
  }

  post (url, data){
    return this.request(url, 'POST', data);
  }

  put (url, data) {
    return this.request(url, 'PUT', data);
  }

  del (url) {
    return this.request(url, 'DELETE');
  }

  patch (url, data) {
    return this.request(url, 'PATCH', data);
  }

  request (url, method, data) {
    const { accessToken } = store.getState().auth;

    const requestUrl = `http://${config.api.host}:${config.api.port}/api${url}`;
    return new Promise((resolve, reject) => {
      console.log('API REQUEST', requestUrl, data);
      fetch(requestUrl, {
        method: method || 'get',
        headers: {
          'Authorization': accessToken,
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: method !== 'GET' && data && JSON.stringify(data) || undefined
      })
        .then((response) => {
          return response;
        })
        .then((response) => {
          if (method !== 'DELETE') {
            return response.json();
          }
          return {};
        })
        .then((response) => {
          console.log('API RESPONSE', response);
          return response;
        })
        .then(resolve)
        .catch((error) => {
          throw error;
        })
        .catch(reject);
    });
  };
}

export default new ApiService();
