import React, {Component, Fragment} from 'react';
import {View,  Image} from 'react-native';

export const Socket = (props) => {
    const {state, error, sizes} = props;

    const  renderIcon = () => {

        if(error){
            return  <Image style={{...sizes}} source={require('../../assets/icons/carriage/socket-err.png')}/>
        }
        if(state){

            return <Image style={{...sizes}} source={require('../../assets/icons/carriage/socket-active.png')}/>
        } else {
            return <Image style={{...sizes}} source={require('../../assets/icons/carriage/socket-def.png')}/>
        }
    };
    return (
        <View>
            {renderIcon()}
        </View>
    )
};
