import {createSwitchNavigator, createAppContainer} from "react-navigation"
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import {Text,Image,View} from 'react-native';

import InitialRoute from './initialRoute';
import  StartScreen from './src/screens/StartScreen'
import AuthScreen from './src/screens/authScreen';
import NotificationScreen from './src/screens/NotificationScreen';
import HomeScreen from './src/screens/HomeScreen';
import SettingsScreen from './src/screens/SettingsScreen';
import React from 'react';
import MonitoringScreen from './src/screens/MonitoringScreen';
import SystemScreen from './src/screens/SystemScreen';
import Humidity from "./src/components/Humidity";
import Thermometer from "./src/components/Thermometer";


const SignedIn = createBottomTabNavigator(
    {
        NotificationTab: {
            screen: NotificationScreen,
            navigationOptions : {
                title: '',
                tabBarIcon: ({ focused }) => {
                    return (
                        <View>
                            {focused ? (
                                <Image style={{height:20,width:20}} source={require('./assets/icons/menu/notifications-active.png')}/>
                            ) : (
                                <Image style={{height:20,width:20}} source={require('./assets/icons/menu/notifications.png')}/>
                            )}
                        </View>
                    );
                }

            },
        },
        HomeTab: {
            screen: HomeScreen,
            navigationOptions : {
                title: '',
                tabBarIcon: ({ focused }) => (
                    <View >
                        {focused ? (
                            <Image style={{height:20,width:20}} source={require('./assets/icons/menu/home-active.png')}/>
                        ) : (
                            <Image style={{height:20,width:20}} source={require('./assets/icons/menu/home.png')}/>
                        )}
                    </View>
                )
            },
        },
        SettingsTab: {
            screen: SettingsScreen,
            navigationOptions : {
                title: '',
                tabBarIcon: ({ focused }) => (
                    <View>
                        {focused ? (
                            <Image style={{height:20,width:20}} source={require('./assets/icons/menu/settings-active.png')}/>
                        ) : (
                            <Image style={{height:20,width:20}} source={require('./assets/icons/menu/settings.png')}/>
                        )}
                    </View>
                ),
            },
        },
    },
    {
        initialRouteName: 'HomeTab',
        navigationOptions : {
            header: null,
            headerMode: 'none'
        },
        barStyle: {backgroundColor: 'white'},
        tabBarOptions: {
            showLabel: false,
            style:{
                // borderTopColor: "transparent",

                // paddingBottom:1,
                // padding:0

            },
            labelStyle: {
                margin: 0
            },
        },
    },


);



const SignOutStack = createStackNavigator(
    {
        Auth: AuthScreen,
        Start: StartScreen

    },
    {
        initialRouteName: 'Start',
    }
);

const HomeSystemStack = createStackNavigator(
    {
        Main:{
            screen: HomeScreen,
            navigationOptions : {
                title: 'Home',
                tabBarIcon: ({ focused }) => (
                    <Icon name="home" size={20} style={{opacity: focused ? 1 : 0.6}} color="#3e2465"/>
                )
            },
        },
        System:{
            screen: SystemScreen
        }
    },
    {initialRouteName:'Main'}
)

const AppStack = createStackNavigator({
    Tabs: SignedIn,
    HumidityTab: {
        screen: Humidity,
        navigationOptions: {header: null,
            headerMode: 'none'},
    },
    ThermometerTab: {
        screen: Thermometer,
        navigationOptions: {header: null,
            headerMode: 'none'},

    },
    System: {screen: SystemScreen,navigationOptions: {header: null,
            headerMode: 'none'},},
}, {
    initialRouteName: 'Tabs',
    inactiveColor: '#ffc580',


})


export default createAppContainer(
    createSwitchNavigator(
        {
            AuthLoading: InitialRoute,
            Start: StartScreen,
            App: AppStack,
            Auth: AuthScreen,
        },
        {
            initialRouteName: 'AuthLoading',
        }
    )
);

