import React, {Component, Fragment} from 'react';
import {StyleSheet, View} from "react-native";
import {UserBlock} from "../../components/userBlock";
import {FastSettings} from "../../components/fastSettings";
import {ListSettings} from "../../components/ListSettings";

export default class Settings extends Component {
    render() {
        return (
            <View style={styles.container}>
                <UserBlock/>
                <FastSettings/>
                <View style={styles.dividerBold}></View>
                <ListSettings/>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
    },
    dividerBold: {
        width: '100%',
        height: 3,
        backgroundColor: '#f5f5f5'
    }
});
