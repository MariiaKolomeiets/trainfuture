/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component, Fragment} from 'react';


import Start from "../containers/start"

export default class StartScreen extends Component {

    render() {

        return (
            <>
               <Start navigation={this.props.navigation}/>
            </>
        );
    }
}


