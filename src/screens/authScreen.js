/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component, Fragment} from 'react';

import Auth from "../containers/authorization"

export default class AuthScreen extends Component {

    render() {

        return (
            <>
               <Auth/>
            </>
        );
    }
}


