import React, {Component, Fragment} from 'react';
import {View, Text, StyleSheet, FlatList, TouchableWithoutFeedback, Image, ScrollView} from 'react-native';
import {SocketUtils} from '../../utils/socket';
import {setHumidity, setCarriageTemp} from '../../store/actions'

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"
import { bindActionCreators } from "redux";
import {connect} from "react-redux";
import LinearGradient from 'react-native-linear-gradient';
import Carousel,  { Pagination } from 'react-native-snap-carousel';
import {InfoCarriage, InfoPower, InfoSections} from "../../widgets/infoCarriage";

import { Container, Header, Content, Icon, Picker, Form } from "native-base";

class Home extends Component {
    constructor(props) {
        super();
        const {externalTemperature, internalTemperature, carriageTemperature} = props;
        this.state = {
            i_temp: internalTemperature,
            e_temp: externalTemperature,
            c_temp: carriageTemperature,
            activeSlide:0,
            carriage: undefined,
            train: undefined
        };
    }


    componentDidMount() {
        console.log(this.props, 'props')
        const { setCarriageTempAction, setHumidityAction } = this.props;
        console.log("SocketService",SocketUtils)
        SocketUtils.socketEmmiter('emmit_temperature', setCarriageTempAction);
        SocketUtils.socketEmmiter('emmit_humidity', setHumidityAction);
    }

    navigateTo = (system) => {
        const {navigation} = this.props;
        navigation.navigate('System', {system});
    };


    renderItemList = ({item, index}) => {
        const icon = this.getIcon(item.icon);
        return (
            <TouchableWithoutFeedback onPress={() => this.navigateTo(item.icon)}>
                <View style={styles.systemContainer}>
                    <View style={styles.systemIconWrap}>
                        <Image style={styles.systemIcon} source={icon}/>
                    </View>
                    <View>
                        <Text style={styles.systemText}>{item.title}</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    };

    getIcon = (icon) => {
        console.log(icon);

        switch (icon) {
            case 'thermometer':
                return require('../../../assets/icons/home/thermometer.png');
            case 'power':
                return require('../../../assets/icons/home/power.png');
            case 'humidity':
                return require('../../../assets/icons/home/humidity.png');
            case 'devices':
                return require('../../../assets/icons/home/devices.png');
            case 'brightness':
                return require('../../../assets/icons/home/brightness.png');
            default:
                return require('../../../assets/icons/home/thermometer.png');
        }

    };
    _renderItemCarousel = (item) => {
        console.log(item,'item')
        switch (item.item) {
            case 'info':
                return <InfoCarriage c_temp={this.props.carriageTemperature} e_temp={this.props.externalTemperature} humidity={this.props.humidity}/>;
            case 'power':
                return <InfoPower/>;
            case 'carriage':
                return <InfoSections/>
            default:
                return <InfoCarriage/>;

        }
    };
    get pagination () {
        const {  activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={entries.length}
                activeDotIndex={activeSlide}
                containerStyle={{height:wp(10),paddingHorizontal:5,paddingVertical:5}}
                dotStyle={{
                    width: 7,
                    height: 7,
                    borderRadius: 5,
                    marginHorizontal: 8,
                    backgroundColor: '#ffc580'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                }}
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
            />
        );
    }

    onValueChange(field: string,value: string) {
        this.setState({
           [field]: value
        });
    }
    render() {
        const {externalTemperature, internalTemperature, carriageTemperature, humidity} = this.props;

        return (
            <>
                <View style={styles.container}>
                    <LinearGradient colors={['#ffdcb3', '#ffd19a','#ffc580']} style={styles.linearGradient}>
                        <View style={styles.headerSection}>
                            <View style={styles.greetingUser}>
                                <Picker
                                    mode="dropdown"
                                    placeholder="Select the train ﹥"
                                    placeholderStyle={{ color: "#373027" }}
                                    style={{ width: undefined }}
                                    selectedValue={this.state.train}
                                    onValueChange={(value)=>{this.onValueChange('train', value)}}
                                >
                                    <Picker.Item label="624A" value="key0" />
                                    <Picker.Item label="15K" value="key1" />

                                </Picker>
                                <Picker
                                    mode="dropdown"
                                    placeholder="Select the carriage ﹥"
                                    placeholderStyle={{ color: "#373027" }}
                                    style={{ width: undefined }}
                                    selectedValue={this.state.carriage}
                                    onValueChange={(value)=>{this.onValueChange('carriage', value)}}
                                >
                                    <Picker.Item label="1" value="key0" />
                                    <Picker.Item label="2" value="key1" />
                                    <Picker.Item label="2" value="key2" />
                                    <Picker.Item label="4" value="key3" />
                                    <Picker.Item label="5" value="key4" />
                                    <Picker.Item label="6" value="key3" />
                                    <Picker.Item label="7" value="key4" />
                                    <Picker.Item label="8" value="key4" />
                                    <Picker.Item label="9" value="key3" />
                                    <Picker.Item label="10" value="key4" />
                                </Picker>
                            </View>
                            {/*<View style={styles.currentRouteContainer}>*/}
                                {/*<Text style={styles.routeText}> Kharkiv - Lviv</Text>*/}
                            {/*</View>*/}

                            {/*<View style={styles.indicatorContainer}>*/}
                            {/*<View style={styles.indicatorsTemperature}>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}>*/}
                            {/*<Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/thermometer-small.png')}/></View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>{externalTemperature} °C</Text>*/}
                            {/*<Text style={styles.greyText}>External</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}><Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/thermometer-small.png')}/></View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>{internalTemperature} °C</Text>*/}
                            {/*<Text style={styles.greyText}>Internal</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}><Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/thermometer-small.png')}/></View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>{carriageTemperature} °C</Text>*/}
                            {/*<Text style={styles.greyText}>Carriage</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}>*/}
                            {/*<Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/humidity-small.png')}/>*/}
                            {/*</View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>{humidity} %</Text>*/}
                            {/*<Text style={styles.greyText}>Humidity</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}>*/}
                            {/*<Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/electric.png')}/></View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>0 A </Text>*/}
                            {/*<Text style={styles.greyText}>Generator</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}><Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/electric.png')}/></View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>-6 A</Text>*/}
                            {/*<Text style={styles.greyText}>Battery</Text>*/}
                            {/*</View>*/}
                            {/*<View style={styles.indicator}>*/}
                            {/*<View style={styles.temperatureWrap}><Image style={styles.temperatureIcon}*/}
                            {/*source={require('../../../assets/electric.png')}/></View>*/}
                            {/*<Text style={[styles.greyText, styles.temperature]}>86 V</Text>*/}
                            {/*<Text style={styles.greyText}>Voltage</Text>*/}
                            {/*</View>*/}
                            {/*</View>*/}
                            {/*</View>*/}

                        </View>
                    </LinearGradient>
                    <View style={styles.infoBlockWrap}>
                        <Carousel
                            layout={'default'}
                            ref={(c) => { this._carousel = c; }}
                            data={entries}
                            renderItem={this._renderItemCarousel}
                            itemWidth={wp(90)}
                            sliderWidth={wp(100)}
                            onSnapToItem={(index) => this.setState({ activeSlide: index }) }

                        />
                        <View>
                            {this.pagination}
                        </View>

                    </View>

                    <View style={styles.systemsContainer}>
                        <FlatList
                            vertical
                            numColumns={2}
                            columnWrapperStyle={{flexDirection:'row', alignItems:'center', justifyContent:'center', flex: 1}}
                            containerStyle={styles.systemsWrap}
                            data={data}
                            renderItem={this.renderItemList}
                        />

                    </View>
                </View>

            </>
        );
    }
}
const entries = ['info','power','carriage'];

const putActionsToProps = dispatch => {
  return {
    setCarriageTempAction: bindActionCreators(setCarriageTemp, dispatch),
    setHumidityAction: bindActionCreators(setHumidity, dispatch),
  };
};
export default connect(
    state => ({
        externalTemperature: state.main.externalTemperature,
        internalTemperature: state.main.internalTemperature,
        carriageTemperature: state.main.carriageTemperature,
        humidity: state.main.humidity
    }), putActionsToProps
)(Home)
const styles = StyleSheet.create({
    container: {
        height: hp(90),
        backgroundColor:'#fafafa',
        position:'relative'
    },
    systemsWrap: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        alignItems: 'center',

    },
    systemsContainer: {
        height: hp(55),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:wp(25)
    },
    headerSection: {
        height: hp(20),
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        shadowOpacity: 0.2,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },

    },
    infoBlockWrap:{
        position:'absolute',
        top:wp(20),
        left:0,
        height:wp(47),
    },
    infoBlock:{
        backgroundColor:'#fff',
        height:wp(35),
        width:wp(90),
        borderRadius:wp(7),
        shadowColor: 'rgba(0, 0, 0, 0.2)',
        shadowOpacity: 0.8,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },

        zIndex:22
    },
    linearGradient: {
        paddingLeft: 15,
        paddingRight: 15,
        // borderRadius: 5,
        borderBottomLeftRadius: wp(10),
        borderBottomRightRadius: wp(10)
    },
    systemContainer: {
        width: wp(35),
        height: hp(18),
        maxHeight: 304,
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOpacity: 0.2,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        borderRadius: wp(2),
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: wp(5),
        paddingHorizontal: wp(3),
        margin: wp(2),
        backgroundColor: '#fff',

    },
    systemText: {
        color: '#373027',
        fontFamily: 'Lato',
        fontSize: wp(4),
        paddingTop: wp(2)
    },
    greetingUser: {
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    currentRouteContainer: {
        paddingBottom: 10,
        paddingLeft: 10,
        width:'100%'
    },
    routeText: {
        fontFamily: 'Lato-Bold',
        fontFamily: 'Oswald-Regular',
        fontSize: wp(5.4),
        color: '#373027',
        textAlign:'center'
    },
    indicator: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: wp(20),
        height: wp(20),
        backgroundColor: '#fff',
        paddingHorizontal: 7,
        paddingVertical: 7,
        borderRadius: wp(3),
        shadowColor: 'rgba(0, 0, 0, 0.25)',
        shadowOpacity: 0.2,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        position: 'relative',
        overflow: 'hidden',
        marginBottom: wp(5)
    },
    temperatureWrap: {
        position: 'absolute',
        top: 10,
        left: -25
    },
    temperatureIcon: {
        width: wp(16),
        height: wp(16),
        opacity: 0.3

    },
    indicatorContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 40,
        paddingRight: 40,
    },
    indicatorsTemperature: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
    indicatorState: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    systemIcon: {
        height: wp(8),
        width: wp(8),
        marginBottom: wp(3)
    },
    systemIconWrap: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center',
        width: '100%'
    },
    greeting: {
        fontFamily: 'Lato-Regular',
        fontSize: wp(4.4),
        color: '#373027'
    },
    currentRoute: {
        fontFamily: 'Oswald-Regular',

        fontSize: wp(4.4),
        color: '#373027'
    },
    greyText: {
        fontFamily: 'Oswald-Regular',

        fontSize: wp(4),
        color: '#373027'
    },
    temperature: {
        fontSize: wp(6)
    }
});
const data = [
    {
        title: 'Termostate',
        icon: 'termostate',
    },
    {
        title: 'Power supply',
        icon: 'power',
    },
    // {
    //     title: 'Tech Info',
    //     icon: 'power',
    // },
    {
        title: 'Devices',
        icon: 'devices',
    },
    {
        title: 'Brightness',
        icon: 'brightness'
    }
];


