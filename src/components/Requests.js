
import React, {Component, Fragment} from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback, FlatList, Image} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"
export  const Requests = () => {
    const renderItem = (item) => {
        console.log(item)
        return (
            <View style={styles.errorContainer}>
                <View style={styles.infoContainer}>
                    <View style={styles.problem}>
                        <View style={styles.textRowContainer}>
                            <Text style={styles.sectionNumber}>Train 624</Text>
                        </View>
                        <View style={styles.textRowContainer}>
                            <Text style={styles.sectionNumber}>Section {Math.floor(Math.random() * 11)+1}: </Text>
                            <Text> {item.item.error}.</Text>
                        </View>
                    </View>
                    <View style={styles.sendReq}>
                        <Image style={styles.sendIcon} source={require('../../assets/icons/notifications/information.png')}/>
                    </View>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.errorsNotificationsContainer}>
                <FlatList
                    numColumns={1}
                    containerStyle={styles.systemsWrap}
                    data={orders}
                    renderItem={renderItem}
                />

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        height: hp(85)
    },
    errorsNotificationsContainer:{
        width:'80%',
    },
    errorContainer:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:5,
        paddingVertical:5,
        marginVertical:7,
        borderRadius:4,
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOpacity: 0.2,
        shadowRadius: 8,
        shadowOffset: {
            width: 1,
            height: 6
        },
        backgroundColor:'#fff',
    },
    textRowContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        width:'100%'

    },
    sectionNumber:{
        fontFamily: 'Oswald-Regular',

        fontSize: wp(4.4),
        color: '#373027'
    },
    check:{
        fontFamily: 'Lato-Regular',
        fontSize: wp(3.4),
        color: '#373027',
    },
    infoContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width:'100%'
    },
    sendIcon:{
        height:25,
        width:25
    },
    problem:{

    },
    sendReq:{

    }
})
const orders = [
    {
        section: '2',
        error: 'Ventilation error',
    },
    {
        section: '6',
        error: 'Lightening error',
    },
    {
        section: '9',
        error: "Plug don't work",
    },
    {
        section: '2',
        error: 'Ventilation error',
    },
    {
        section: '6',
        error: 'Lightening error',
    },
    {
        section: '9',
        error: "Plug don't work",
    },
    {
        section: '2',
        error: 'Ventilation error',
    },
    {
        section: '6',
        error: 'Lightening error',
    },
    {
        section: '9',
        error: "Plug don't work",
    },

];
