
import React, {Component, Fragment} from 'react';
import {View, StyleSheet, ScrollView,TouchableWithoutFeedback, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Rect, Svg,Text,G} from "react-native-svg";
import {Lock} from '../widgets/lockState';
import {Socket} from '../widgets/socketState';

export default class Carriage extends Component {
    constructor(props){
        super(props)

        this.state = {
            data: data,
            errors: false,
            carriage:{
                vestibule1:true,
                vestibule2: false,
            }
        };
        this.interval = null;
    }

        componentDidMount(){

            // this.props.navigation.addListener("willFocus", () => {
            //     this.interval = setInterval(()=>{
            //         this.setState({errors: ! this.state.errors})
            //     },700)
            // })
            // this.props.navigation.addListener("didBlur", () => {
            //     clearInterval(this.interval)
            // });

        }
     changeStateSpecificRoom = (room,state)=>{
        console.log('INN')
        const {carriage} = this.state;
        let changedObj = carriage;
        changedObj[room] = state;
        console.log(changedObj,'changed');
        this.setState({carriage:{...changedObj}})
     }
    render(){
        const {carriage} = this.state;
        return (
            <View style={styles.carriageContainer}>
                <View style={styles.carriageWrap}>
              <View style={styles.carriage}>
                  <View style={[styles.vestibule, styles.v1]}>

                      <View style={[styles.vestLockPosition, styles.firstVest]}>
                          <TouchableWithoutFeedback onPress={()=>{console.log('In')}}>
                          <Lock state={true} error={false} sizes={{width:wp(5.5), height:wp(5.5)}}/>
                          </TouchableWithoutFeedback>
                      </View>

                  </View>
                  <View style={[styles.bathroom,styles.b1]}>
                      <View style={styles.socket}>
                          <View style={styles.block}>
                              <Socket sizes={{width:wp(5.5), height:wp(5.5)}} state={true} error={false}/>
                          </View>
                      </View>
                      <View style={styles.divider}></View>
                      <View style={styles.WC}>
                          <View style={styles.lockWrap}>
                              <Lock state={true} error={true} sizes={{width:wp(5.5), height:wp(5.5)}}/>
                          </View>
                      </View>
                  </View>

                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={styles.section}>

                  </View>
                  <View style={[styles.section, styles.last]}>

                  </View>
                  <View style={[styles.bathroom, styles.b2]}>
                      <View style={styles.socket}>
                        <View style={styles.block}>
                            <Socket sizes={{width:wp(5.5), height:wp(5.5)}} state={true} error={false}/>
                        </View>
                      </View>
                      <View style={styles.divider}></View>
                      <View style={styles.WC}>
                          <View style={styles.lockWrap}>
                          <Lock state={true} error={false} sizes={{width:wp(5.5), height:wp(5.5)}}/>
                          </View>
                      </View>
                  </View>
                  <View style={[styles.vestibule, styles.v2]}>
                      <View style={[styles.vestLockPosition, styles.secondVest]}>
                          <Lock state={false} error={false} sizes={{width:wp(5.5), height:wp(5.5)}}/>
                      </View>
                  </View>

              </View>
                    <View style={styles.lamps}>
                        <View style={styles.lamp}>
                            <Image style={styles.lampIcon} source={require('../../assets/icons/carriage/lamp-def.png')}/>
                        </View>
                        <View style={styles.lamp}>
                            <Image style={styles.lampIcon} source={require('../../assets/icons/carriage/lamp-def.png')}/>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    carriageContainer: {
        height: '90%',
        width: wp(40),
        marginTop:wp(5),
    },

    carriage:{
        height: wp(130),
        width: wp(40),
        borderWidth:2,
        borderRadius:wp(1),
        borderColor:'#373027',
        flexDirection:'column',
        alignItems: 'flex-end',
        justifyContent:'center'
    },
    lamps:{
        width: wp(40),
        flexDirection:'row',
        justifyContent:'space-between'
    },
    lamp:{
        height:wp(12),
        width:wp(12),
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        borderWidth:2,
        borderRadius:wp(1),
        borderTopColor:'transparent',
        borderColor:'#373027',
    },
    lampIcon:{
        height:wp(7),
        width:wp(7),
    },
    carriageWrap:{

    },
    vestibule: {
        height: wp(7),
        width:'100%',
        borderColor: '#373027',
        position:'relative'
    },
    v1:{
        borderBottomWidth: 2,
    },
    v2:{
        borderTopWidth: 2,
    },
    bathroom: {
        height: wp(12),
        width:'100%',
        borderColor: '#373027',
        flexDirection:'row',
        justifyContent:'space-between',
        zIndex: -1
    },

    b1:{
        borderBottomWidth: 2,
    },
    b2:{
        borderTopWidth: 2,
    },

    section:{
        height: wp(9),
        width:'80%',
        borderBottomWidth: 2,
        borderLeftWidth:2,
        // borderRadius: wp(1),
        borderColor: '#373027',
    },
    last:{
        borderBottomWidth: 0,
    },
    vestLockPosition:{
        height:wp(8),
        width:wp(8),
        borderWidth:2,
        borderRadius:wp(1),
        borderColor:'#373027',
        backgroundColor:'#fafafa',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        position: 'absolute',
        right:'40%',
        zIndex:999
    },
    lockWrap:{
        height:wp(8),
        width:wp(8),
        borderWidth:2,
        borderRadius:wp(1),
        borderColor:'#373027',
        backgroundColor:'#fafafa',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },

    block:{
        height:wp(8),
        width:wp(8),
        borderWidth:2,
        borderRadius:wp(1),
        borderColor:'#373027',
        backgroundColor:'#fafafa',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    firstVest:{
        bottom:-wp(4),

    },
    secondVest:{
        top:-wp(4),
    },
    socket:{
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    WC:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        flex:1
    },
    divider: {
        height: wp(12),
        width:wp(0.5),
        backgroundColor: '#373027',


    }
});

const data = [
    {
        section: 1,
        errors:['test','test']
    },
    {
        section: 2,
        errors:[]
    },
    {
        section: 3,
        errors:[]
    },

    {
        section: 4,
        errors:[]
    },
    {
        section: 5,
        errors:[]
    },
    {
        section: 6,
        errors:[]
    },
    {
        section: 6,
        errors:[]
    }

]
