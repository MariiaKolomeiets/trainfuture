import React, {Component, Fragment} from 'react';
import {View, Text, StyleSheet,FlatList, Modal, TouchableHighlight} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"
import Carriage from '../components/Carriage';

export default class MonitoringScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showInfoModal: false,
            info: null
        }
    }

    getInfoAboutCarriage = (info) =>{
        this.setState({showInfoModal: true, info})
    }
    closeModal = ()=> {
        this.setState({showInfoModal: false})
    }
    render() {
        const {info} = this.state;

        return (
            <>
                <View style={styles.container}>
                    <View style={styles.carriage}>
                    <Carriage {...this.props} showInfo={this.getInfoAboutCarriage}/>
                    </View>
                </View>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showInfoModal}
                    onRequestClose={() => {
                    }}>

                    <View style={styles.modalWrap}>
                        <View style={styles.closeModal}>
                            <TouchableHighlight
                                onPress={() => {
                                    this.closeModal();
                                }}>
                                <Text>Hide Modal</Text>
                            </TouchableHighlight>
                        </View>
                        <View>
                            {info? (<View>
                                <Text style={{fontSize:20}}>Section: {info.section}</Text>
                                <Text style={{fontSize:20}} >Errors: {info.errors.length}</Text>
                            </View>): null}
                        </View>
                    </View>
                </Modal>

            </>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: hp(90)
    },
    modalWrap:{
        justifyContent: "flex-start",
        alignItems: "center",
        flexDirection: "column",
        height: "50%",
        width: "80%",
        position: "absolute",
        left: 40,
        top: 100,
        backgroundColor:'#fff'
    },
    closeModal:{
        width:'100%',
        flexDirection:'row',
        justifyContent: 'flex-end',
        paddingVertical: 10,
        paddingRight:10
    },
    carriage:{
        paddingHorizontal:10,
        paddingVertical:10
    }
});
const data = [
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
    {
        title: 'Ventilation',
        icon: 'thermometer-three-quarters',
    },
];
