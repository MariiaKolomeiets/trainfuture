import React, {Component, Fragment} from 'react';
import {Form, Item, Input, Label, Button} from 'native-base';
import {userService} from "../../utils/user";
import {
    StyleSheet,
    View,
    Text,
    Animated,
    Image,
    Easing,
    TouchableWithoutFeedback
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen"
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {setUser} from "../../store/actions";


class Start extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: ''
        };
        this.regex =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    }

    navigate = () => {
        const {navigate} = this.props.navigation;
        navigate('App')
    };

    onChangeText = (field, value) => {
        this.setState({[field]: value});
    };
     validateEmail = (email) =>{
        return this.regex.test(String(email).toLowerCase());
    };

    login = async () => {
        this.navigate();
        return;
        const {setUser} = this.props;
        const {email, password} = this.state;
        if(!email && !password) return;

        if(!this.validateEmail(email) && password.length < 4) return;
        try{
            const user = await userService.auth({email,password});
            setUser(user.user);
            this.navigate();

        } catch (e) {
            console.log(e);
        }

    };

    render() {
        const {email, password} = this.state;
        return (
            <>
                <View style={styles.container}>
                    <View style={styles.headerContainer}>
                        <View style={styles.header}>
                            <Image style={styles.logo} source={require('../../../assets/management.png')}/>
                        </View>
                    </View>
                    <View>
                        <View style={styles.descriptionContainer}>
                            <Text style={[styles.descriptionText, styles.mainText]}>Login info train-future</Text>
                            <Text style={[styles.descriptionText, styles.additionalText]}>Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation</Text>
                        </View>
                        <View style={styles.formContainer}>
                            <Form style={styles.signInForm}>
                                <Item floatingLabel style={styles.inputContainer}>
                                    <Label style={[styles.formFonts, styles.SignInLabel]}>Username</Label>
                                    <Input style={[styles.SignInInput, styles.formFonts]} value={email}
                                           onChangeText={(value) => {
                                               this.onChangeText('email', value)
                                           }}/>
                                </Item>
                                <Item floatingLabel style={styles.inputContainer}>
                                    <Label style={[styles.formFonts, styles.SignInLabel]}>Password</Label>
                                    <Input style={[styles.SignInInput, styles.formFonts]} value={password}
                                           onChangeText={(value) => {
                                               this.onChangeText('password', value)
                                           }}/>
                                </Item>
                                <View style={styles.signInBtn}>
                                    <Button light style={styles.btn} onPress={()=> this.login()}><Text
                                        style={styles.btnText}> Sign In </Text></Button>
                                </View>
                            </Form>

                        </View>
                    </View>
                </View>
            </>
        );
    }
};
const putActionsToProps = dispatch => {
    return {
        setUser: bindActionCreators(setUser, dispatch),
    };
};

export default connect(
    null,
    putActionsToProps
)(Start)

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: hp(100),
        backgroundColor: '#fff'
    },
    captionHeader: {
        textAlign: 'center',
        fontSize: 17,
        fontWeight: 'bold',
        color: '#9079F0'
    },
    descriptionText: {
        fontFamily: 'Lato-Regular',


    },
    mainText: {
        fontFamily: 'Oswald-Regular',
        fontSize: wp(5),
        textAlign: 'center',
        color: '#ffc580'
    },
    additionalText: {
        paddingVertical: 10,
        paddingHorizontal: 20,
        textAlign: 'center'
    },
    descriptionContainer: {},
    captionContatiner: {
        height: hp(20),
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerContainer: {
        height: hp(35),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',

    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    logo: {
        height: wp(45),
        width: wp(45),
    },
    logoContainer: {
        position: 'absolute',
        top: 5,

    },
    logoTextContainer: {
        position: 'absolute',
        right: 75,
    },
    logoText: {
        fontSize: 17,
        fontWeight: 'bold',
        //color:'#9079F0'
    },
    btnContainers: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: hp(20)
    },
    divider: {
        height: wp(10),
        width: wp(0.5),
        backgroundColor: '#9079F0',
        marginLeft: wp(1),
        marginRight: wp(1)
    },
    text: {
        fontSize: 15,
        fontWeight: 'bold',
    },
    signInForm: {
        width: wp(80),
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    formContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    signInBtn: {
        width: '95%',
        paddingTop: 20,
        paddingBottom: 20,
    },
    btn: {
        borderRadius: wp(30),
        height: wp(10),
        justifyContent: 'center',
        marginTop: wp(7),
        backgroundColor: '#ffc580',
    },
    btnText: {
        fontFamily: 'Oswald-Regular',
        fontSize: wp(4.4),
        color: '#fff',

    },
    formFonts: {
        fontFamily: 'Lato-Regular',
        fontSize: wp(4),
    },
    SignInInput: {
        color: '#373027',

        // color:'#ffc580'

    },
    SignInLabel: {
        color: '#bababa',
        fontFamily: 'Oswald-Regular',

    },
    inputContainer: {}
});


