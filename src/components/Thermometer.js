import React, {Component, Fragment} from 'react';
import {View, Text, Image, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import CircularSlider from './CircleSlider';
import Slider from '@react-native-community/slider';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {setCarriageTemp,setWindSpeed, changeTermostateMode} from "../store/actions";

class Thermometer  extends Component{
    constructor(props) {
        super(props);

        this.timerTemp = null;
        this.timerWind = null;
        const {carriageTemperature, windSpeed,termostateMode} = props;
        this.state = {
            slider1: Number(carriageTemperature*9).toFixed(0),
            cool: termostateMode === 'cool',
            wind:windSpeed
        };
    }

    changeValue = (value) => {
        const {setCarriageTemp} = this.props;
        if(value < 60 || value > 300) return;

        clearTimeout(this.timerTemp);

        this.timerTemp = setTimeout(() => {
            const tempValue = Number((value/9).toFixed(0));
            setCarriageTemp(tempValue);
        }, 300);
        this.setState({slider1: value});

    };

    changeMode = (cool) => {
        const {changeTermostateMode} = this.props;
        changeTermostateMode(cool ? 'cool': 'warm');
        this.setState({cool});

    };

    changeWindSpeed = (value) => {
       const {setWindSpeed} = this.props;
        clearTimeout(this.timerTemp);

        this.timerTemp = setTimeout(() => {
            setWindSpeed(value);
        }, 300);
        this.setState({wind: value});
    }
    render(){
        const {windSpeed, termostateMode} = this.props;
        console.log(windSpeed)
        return(
            <View style={{height:hp(70), flexDirection:'column', alignItems:'center', justifyContent:'space-between'}}>
                <CircularSlider width={200} height={200} type='temperature' meterColor='#0cd' textColor='#fff'
                                value={this.state.slider1} onValueChange={this.changeValue}/>
                <View style={styles.temperatureMode}>
                    <View style={styles.itemWrap}>
                        <TouchableWithoutFeedback onPress={()=>this.changeMode(false)}>
                        <View style={[styles.stateCircle, this.state.cool ? null : styles.active]}>
                            {this.state.cool ? (<Image style={styles.stateIcon} source={require('../../assets/icons/warm.png')}/>):(
                                <Image style={styles.stateIcon} source={require('../../assets/icons/warm-active.png')}/>
                            )}
                        </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.modeText}>Warm</Text>
                    </View>
                    <View style={styles.itemWrap}>
                        <TouchableWithoutFeedback onPress={()=> this.changeMode(true)}>
                        <View style={[styles.stateCircle, this.state.cool ?  styles.active: null]}>
                            {this.state.cool ? (
                                <Image style={styles.stateIcon} source={require('../../assets/icons/cold-active.png')}/>
                            ):(
                                <Image style={styles.stateIcon} source={require('../../assets/icons/cold.png')}/>
                            )}

                        </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.modeText}>Cold</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'column', alignItems: 'center', width:'100%'}}>
                    <Text style={styles.windText}>Wind Speed</Text>
                    <View style={styles.sliderWrap}>
                        <Slider
                            style={{width: wp(65), height: 40,marginRight:20}}
                            value={windSpeed}
                            minimumValue={0}
                            maximumValue={100}
                            step={5}
                            onValueChange={this.changeWindSpeed}
                            minimumTrackTintColor='#ffc580'
                            maximumTrackTintColor="#ffc580"
                        />
                        <Image style={styles.breezeSizes} source={require('../../assets/icons/breeze.png')}/>
                    </View>

                </View>
            </View>
        )
    }


}
const styles = StyleSheet.create({
    sliderWrap:{
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'space-between',
        width:'70%'
    },
    breezeSizes:{
        height:wp(7),
        width: wp(7)
    },
    windText:{
        width:'100%',
        textAlign:'center',
        fontFamily:'Oswald-Regular',
        fontSize:wp(4),
    },
    stateCircle:{
        width:wp(16),
        height:wp(16),
        backgroundColor:'#fff',
        borderRadius:wp(16),
        shadowColor: 'rgba(0, 0, 0, 0.5)',
        shadowOpacity: 0.2,
        shadowRadius: 4,
        shadowOffset: {
            width: 1,
            height: 4,
        },
        flexDirection:'row',
        alignItems:'center',
        justifyContent: 'center'
    },
    stateIcon:{
        width:wp(7),
        height:wp(7),
    },
    active:{
        backgroundColor: '#FFC580'
    },
    temperatureMode:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    itemWrap:{
        width:wp(30),
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center'
    },
    modeText:{
        fontFamily:'Oswald-Regular',
        fontSize:wp(4),
        marginTop:10
    }
});

const putActionsToProps = dispatch => {
    return {
        setCarriageTemp: bindActionCreators(setCarriageTemp, dispatch),
        setWindSpeed: bindActionCreators(setWindSpeed, dispatch),
        changeTermostateMode: bindActionCreators(changeTermostateMode, dispatch)
    };
};
export default connect(
    state =>({
        carriageTemperature: state.main.carriageTemperature,
        windSpeed: state.main.windSpeed,
        termostateMode: state.main.termostateMode
    }),
    putActionsToProps
)(Thermometer)
