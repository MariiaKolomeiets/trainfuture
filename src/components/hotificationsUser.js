
import React, {Component, Fragment} from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback, FlatList} from 'react-native';
import {heightPercentageToDP as hp} from "react-native-responsive-screen";

export  const NotificationsUser = () => {

    const renderItem = (item) => {
      return (
          <View style={styles.userOrderContainer}>
              <View style={styles.iconContainer}>
              </View>
              <View style={styles.infoContainer}>
                  <View style={styles.textRowContainer}>
                      <Text>Passanger: </Text>
                      <Text> Mariia Kolomeiets</Text>
                  </View>
                  <View style={styles.textRowContainer}>
                      <Text>Carriage: </Text>
                      <Text> 10</Text>
                  </View>
                  <View  style={styles.textRowContainer}>
                      <Text>Order: </Text>
                      <Text> 1 Tea with 1 Sugar</Text>
                  </View>
              </View>
          </View>
      )
    }
    return (
        <View style={styles.container}>
            <View style={styles.userNotificationsContainer}>
                <FlatList
                    numColumns={1}
                    containerStyle={styles.systemsWrap}
                    data={orders}
                    renderItem={renderItem}
                />

            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        height: hp(78)
    },
    userNotificationsContainer:{
        width:'80%',
    },
    userOrderContainer:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:5,
        paddingVertical:5,
        marginVertical:7,
        borderRadius:4,
        shadowColor: "rgba(0, 0, 0, 0.5)",
        shadowOpacity: 0.2,
        shadowRadius: 8,
        shadowOffset: {
            width: 1,
            height: 6
        },
        backgroundColor:'#fff',
    },
    textRowContainer:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',

    },
    iconContainer:{
        flex:1
    },
    infoContainer:{
        flex:4
    }
})

const orders = [
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },
    {
        namePassanger: 'Mariia Kolomeiets',
        carriage: 10,
        order: '1 Tea with 1 Sugar',
    },

];
