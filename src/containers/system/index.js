import React, {Component, Fragment} from 'react';
import {StyleSheet, View} from 'react-native';
import Thermometer from '../../components/Thermometer';
import PowerSupply from '../../components/PowerSupply'
import {heightPercentageToDP as hp, wp} from 'react-native-responsive-screen';
import Humidity from '../../components/Humidity';
import Devices from '../../components/Devices';
import Brightness from '../../components/Brightness';
import Thermostate from '../../components/Thermostate';

export const System = (props) => {

    const checkSystem = () =>{

        switch (props.system) {
            case 'thermometer':
                return <Thermostate/>;
            case 'power':
                return <PowerSupply/>;
            case 'humidity':
                return <Humidity/>;
            case 'devices':
                return <Devices/>;
            case 'brightness':
                return <Brightness/>;
            default:
                return <Thermostate/>;
        }
    };
    return(

        <View style={styles.container}>
            {checkSystem()}
        </View>
    )
}
const styles = StyleSheet.create({
 container:{
     width:'100%',
     height:'100%'
 }
})
