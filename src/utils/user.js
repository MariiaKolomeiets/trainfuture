import ApiService from './api'

 const auth = (credentials) =>{
    return ApiService.post('/auth',credentials);
}

export  const userService = {
    auth
}
