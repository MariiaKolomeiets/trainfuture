import React, {Component, Fragment} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Home from '../containers/home';
import SystemScreen from './SystemScreen';

const HomeStack = createStackNavigator({
        Home: {screen: Home},
        System: {screen: SystemScreen},
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
    });

// const HomeContainer = createAppContainer(HomeStack);

export default class HomeScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {

        }
    }

    render() {

        return (
            <>
                <Home navigation={this.props.navigation}/>
            </>
        );
    }
}
