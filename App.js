import React, {Component, Fragment} from 'react';
import {SafeAreaView, Text} from 'react-native';
import {
    StyleSheet,

} from 'react-native';


import SwitchNavigator from './AppNavigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {SocketUtils} from './src/utils/socket';

import { Provider } from "react-redux"
import {store,persistor} from './src/store';
import { PersistGate } from 'redux-persist/integration/react'


Icon.loadFont();

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            isReady: false,
            auth: false,
        };
    }

    componentDidMount() {
        SocketUtils.connect();
    }

    async componentWillMount() {
        this.setState({isReady: true});
    }

    render() {
        const {auth} = this.state;
        if (!this.state.isReady) {
            return null;
        }


        return (
            <>

                <SafeAreaView style={{flex: 1}}>
                    <Provider store={store}>
                        <PersistGate loading={null} persistor={persistor}>
                            <SwitchNavigator/>
                        </PersistGate>
                    </Provider>
                </SafeAreaView>
            </>
        );
    }
}

const styles = StyleSheet.create({});

