import React, {Component, Fragment} from 'react';
import {StyleSheet, Text, View, Image, Switch} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { Container, Header, Content, Form, Item, Picker } from 'native-base';
export class ListSettings  extends Component{

    constructor (props){
        super();
        this.state = {
            selected2: undefined
        };
    }


     onValueChange2 = (value: string) => {
        this.setState({
            selected2: value
        });
    };

    render(){
        return (
            <View style={styles.blockWrap}>
                <View style={styles.pickerWrap}>
                    <Text style={styles.text}>Notifications</Text>
                    <Item picker>
                        <Picker
                            mode="dropdown"
                            style={{ width: undefined }}
                            placeholder="off"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            selectedValue={this.state.selected2}
                            onValueChange={this.onValueChange2}
                        >
                            <Picker.Item label="On" value="key0" />
                            <Picker.Item label="Off" value="key1" />
                        </Picker>
                    </Item>
                </View>
                <View style={styles.dividerBold}></View>
                <View style={styles.pickerWrap}>
                    <Text style={styles.text}>Change Password</Text>
                    <Image  style={styles.navArrow} source={require('../../assets/icons/settings/right-chevron.png')}/>
                </View>
                <View style={styles.dividerBold}></View>
                <View style={styles.pickerWrap}>
                    <Text style={styles.text}>Widget Displaying Settings</Text>
                    <Image  style={styles.navArrow} source={require('../../assets/icons/settings/right-chevron.png')}/>
                </View>
                <View style={styles.dividerBold}></View>
                <View style={styles.pickerWrap}>
                    <Text style={styles.text}>Route reminders</Text>
                    <Image  style={styles.navArrow} source={require('../../assets/icons/settings/right-chevron.png')}/>
                </View>
            </View>
        )
    }

};
const styles = StyleSheet.create({
    blockWrap: {
        width: '100%',
        height: '100%',
        paddingHorizontal: 20
    },
    pickerWrap: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
    },
    text: {
        fontFamily: 'Oswald-Regular',
        fontSize: wp(4),
        color: '#373027',
        flex: 1,
        textAlign: 'left'
    },
    picker:{
        flex: 1,
        fontFamily: 'Lato-Bold',
        fontSize: wp(6.4),
        color: '#ffc580',
        textAlign:'right'
    },
    dividerBold: {
        width: '100%',
        height: 1,
        backgroundColor: '#f5f5f5'
    },
    navArrow:{
        width:wp(5),
        height:wp(5)
    }

})
